package com.mdpcolocado.checkoutcounter.promos;

public enum PromoEnum {
	BOGO,
	DISCOUNTED,
	CUSTOM;
}
