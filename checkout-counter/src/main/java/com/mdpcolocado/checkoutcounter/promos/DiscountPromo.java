package com.mdpcolocado.checkoutcounter.promos;

import com.mdpcolocado.checkoutcounter.utils.DBUtils;

public class DiscountPromo implements IDiscountPromo {
	
	private long productId;
	private double discount;
	private float discountPercentage;
	private Double promoDiscount;
	private String description;
	
	/**
	 * @param productId
	 * @param discount
	 * @param discountPercentage
	 * discount would always be prioritize on computation
	 * if discount percentage also has a value
	 * the discount would be deducted to the actual price first before the discountPercentage 
	 */
	public DiscountPromo(long productId, double discount, float discountPercentage, String description) {
		this.productId = productId;
		this.discount = discount;
		this.discountPercentage = discountPercentage;
		this.description = description;
	}
	
	private DiscountPromo(DiscountPromo discountPromo) {
		// TODO Auto-generated constructor stub
		this.productId = discountPromo.productId;
		this.discount = discountPromo.discount;
		this.discountPercentage = discountPromo.discountPercentage;
		this.description = discountPromo.description;
	}

	@Override
	public double getDiscount() {
		// TODO Auto-generated method stub
		if (promoDiscount == null) {
			promoDiscount = 0d;
			promoDiscount += discount;
			if (discountPercentage > 0f) {
				promoDiscount += DBUtils.getInstance().getProduct(productId).getPrice() * (discountPercentage / 100);
			}
		}
		
		return promoDiscount;
	}

	@Override
	public IPromotion getDeepCopy() {
		// TODO Auto-generated method stub
		return new DiscountPromo(this);
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.description;
	}

}
