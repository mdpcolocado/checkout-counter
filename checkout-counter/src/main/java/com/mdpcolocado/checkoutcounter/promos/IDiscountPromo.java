package com.mdpcolocado.checkoutcounter.promos;

public interface IDiscountPromo extends IPromotion {
	double getDiscount();
}
