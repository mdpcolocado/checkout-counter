package com.mdpcolocado.checkoutcounter.promos;

import java.util.Collection;

import com.mdpcolocado.checkoutcounter.objects.Product;

public interface IScannedProducts {
	void setScannedProducts(Product product, Collection<Product> products);
}
