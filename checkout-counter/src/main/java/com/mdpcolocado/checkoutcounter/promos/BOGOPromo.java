package com.mdpcolocado.checkoutcounter.promos;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.mdpcolocado.checkoutcounter.objects.Product;

/**
 * @author MDPColocado
 *
 *Buy one get one free promotion
 */
public class BOGOPromo implements IDiscountPromo, IScannedProducts {

	private Product product;
	private Double promoDiscount = 0d;
	private String description;
	
	public BOGOPromo(String description) {
		this.description = description;
	}
	
	private BOGOPromo(BOGOPromo bogoPromo) {
		// TODO Auto-generated constructor stub
		this.product = bogoPromo.product;
		this.description = bogoPromo.description;
	}

	@Override
	public double getDiscount() {
		// TODO Auto-generated method stub
		return promoDiscount;
	}
	
	public IPromotion getDeepCopy() {
		return new BOGOPromo(this);
	}

	@Override
	public void setScannedProducts(Product product, Collection<Product> products) {
		// TODO Auto-generated method stub
		this.product = product;
		List<Product> filtered = new LinkedList<>();
		switch(product.getProductUnit()) {
			case PIECE:
				filtered = products.stream()
							.filter(p -> p.getId() == product.getId())
							.collect(Collectors.toList());
				break;
			case KILO:
			case LITER:
				filtered = products.stream()
							.filter(p -> p.getId() == product.getId() && p.getScannedWeightOrVolume() == product.getScannedWeightOrVolume())
							.collect(Collectors.toList());
				break;
		}

		if (filtered.size() % 2 == 0) {
			promoDiscount = product.getPrice();
		} else {
			promoDiscount = 0d;
		}
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.description;
	}
}
