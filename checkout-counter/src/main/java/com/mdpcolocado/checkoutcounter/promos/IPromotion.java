package com.mdpcolocado.checkoutcounter.promos;

public interface IPromotion {
	
	IPromotion getDeepCopy();
	String getDescription();
}
