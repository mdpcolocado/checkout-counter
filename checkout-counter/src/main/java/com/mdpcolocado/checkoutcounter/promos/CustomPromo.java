package com.mdpcolocado.checkoutcounter.promos;

import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

import com.mdpcolocado.checkoutcounter.objects.Product;

public class CustomPromo implements IDiscountPromo, IScannedProducts{
	
	private String description;
	private double discount;
	private float discountPercentage;
	private int freeItemLimit;
	
	private Product product;
	private Deque<Product> scannedProducts;
	
	private boolean active = false;
	
	public static class Builder {
		
		private final String description;
		
		private double discount = 0;
		private float discountPercentage = 0;
		
		private int freeItemLimit;
		
		public Builder(String description) {
			this.description = description;
		}
		
		public FreeItemBuilder getFree() {
			return new FreeItemBuilder(this);
		}
		
		public PromoBuilder discount(double val) {
			this.discount = val;
			return new PromoBuilder(this); 
		}
		
		public PromoBuilder discountPercentage(float val) {
			this.discountPercentage = val;
			return new PromoBuilder(this);
		}
	}
	
	public static class FreeItemBuilder {
		
		private final Builder builder;
		
		private FreeItemBuilder(Builder builder) {
			this.builder = builder;
		}
		
		/**
		 * @param limit - free item limit
		 * @return
		 */
		public PromoBuilder limitTo(int limit) {
			this.builder.freeItemLimit = limit;
			return new PromoBuilder(builder);
		}
		
	}
	
	public static class PromoBuilder {
		
		private final Builder builder;
		
		private PromoBuilder(Builder builder) {
			this.builder = builder;
		}
		
		public CustomPromo build() {
			return new CustomPromo(builder);
		}
	}
	
	private CustomPromo(Builder builder) {
		this.description = builder.description;
		this.discount = builder.discount;
		this.discountPercentage = builder.discountPercentage;
		this.freeItemLimit = builder.freeItemLimit;
	}
	
	public CustomPromo(CustomPromo customPromo) {
		this.description = customPromo.description;
		this.discount = customPromo.discount;
		this.discountPercentage = customPromo.discountPercentage;
		this.freeItemLimit = customPromo.freeItemLimit;
	}
	
	@Override
	public IPromotion getDeepCopy() {
		// TODO Auto-generated method stub
		return new CustomPromo(this);
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.description;
	}

	@Override
	public double getDiscount() {
		// TODO Auto-generated method stub
		
		double val = 0;
		
		if (discount > 0) {
			val = discount;
			active = true;
		}
		if (discountPercentage > 0) {
			val = product.getPrice() * (discountPercentage / 100);
			active = true;
		}
		if (freeItemLimit > 0) {
			List<Product> filtered = scannedProducts.stream()
										.filter(p -> p.getId() == product.getId())
										.collect(Collectors.toList());
			if (filtered.size() % (freeItemLimit + 1) == 0 || 
					(filtered.size() % (freeItemLimit + 1) > 1 && (filtered.size() % (freeItemLimit + 1) < freeItemLimit + 1))) {
				val = product.getPrice();
				active = true;
			} else {
				val = 0d;
			}
		}
		
		return val;
	}

	@Override
	public void setScannedProducts(Product product, Collection<Product> products) {
		// TODO Auto-generated method stub
		this.product = product;
		this.scannedProducts = (Deque<Product>) products;
	}

	public float getDiscountPercentage() {
		return discountPercentage;
	}

	public int getFreeItemLimit() {
		return freeItemLimit;
	}

	public Product getProduct() {
		return product;
	}

	public Deque<Product> getScannedProducts() {
		return scannedProducts;
	}

	public boolean isActive() {
		return active;
	}
	

}
