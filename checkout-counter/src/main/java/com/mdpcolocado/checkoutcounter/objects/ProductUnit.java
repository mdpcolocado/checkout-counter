package com.mdpcolocado.checkoutcounter.objects;

public enum ProductUnit {
	PIECE,
	KILO,
	LITER;
}
