package com.mdpcolocado.checkoutcounter.objects;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.mdpcolocado.checkoutcounter.promos.IDiscountPromo;
import com.mdpcolocado.checkoutcounter.promos.IPromotion;
import com.mdpcolocado.checkoutcounter.utils.DBUtils;

public class Product {
	
	private long id;
	private String name;
	private String description;
	private double price;
	private double promoPrice;
	private double computedPrice;
	private ProductUnit productUnit;
	private List<IPromotion> promotions = new LinkedList<>();
	private float scannedWeightOrVolume = 0;
	
	public static class Builder {
		private final long id;
		private final String name;
		private final double price;
		private final ProductUnit productUnit;
		
		private String description = "";
		private double promoPrice;
		
		public Builder(String name, double price, ProductUnit productUnit) {
			this.id = DBUtils.getInstance().incrementAndGetRecentId();
			this.name = name;
			this.price = price;
			this.productUnit = productUnit;
			this.promoPrice = price;
		}
		
		public Builder(Product product) {
			this.id = product.id;
			this.name = product.name;
			this.price = product.price;
			this.productUnit = product.productUnit;
			this.description = product.description;
			this.promoPrice = product.promoPrice;
		}
		
		public Builder description (String val) {
			this.description = val;
			return this;
		}
		
//		public Builder promoPrice(double val) {
//			this.promoPrice = val;
//			return this;
//		}
		
		public Product build() {
			return new Product(this);
		}
	}
	
	private Product(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.description = builder.description;
		this.price = builder.price;
		this.productUnit = builder.productUnit;
		this.promoPrice = builder.promoPrice;
		this.computedPrice = builder.price;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ProductUnit getProductUnit() {
		return productUnit;
	}

	public void setProductUnit(ProductUnit productUnit) {
		this.productUnit = productUnit;
	}

	public List<IPromotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<IPromotion> promotions) {
		this.promotions = promotions;
	}
	
	public void addPromotions(IPromotion... promos) {
		if (promos.length > 0) {
			this.promotions.addAll(Arrays.asList(promos));
		}
	}

	public double getPromoPrice() {
		promoPrice = price - getPromotions().stream().filter(p -> {
			if (p instanceof IDiscountPromo) {
				return true;
			}
			return false;
		}).mapToDouble(p -> ((IDiscountPromo) p).getDiscount()).sum();
		
		if (promoPrice < 0) {
			promoPrice = 0;
		}
		
		return promoPrice;
	}

	public void setPromoPrice(double promoPrice) {
		this.promoPrice = promoPrice;
	}
	
	public double getComputedPrice() {
		return computedPrice;
	}

	public void setComputedPrice(double computedPrice) {
		this.computedPrice = computedPrice;
	}
	
	public float getScannedWeightOrVolume() {
		return scannedWeightOrVolume;
	}

	public void setScannedWeightOrVolume(float scannedWeight) {
		this.scannedWeightOrVolume = scannedWeight;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price
				+ ", productUnit=" + productUnit + ", promotions=" + promotions + "]";
	}

}
