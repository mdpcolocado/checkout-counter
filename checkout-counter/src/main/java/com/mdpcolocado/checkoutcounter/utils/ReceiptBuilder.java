package com.mdpcolocado.checkoutcounter.utils;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;

import com.mdpcolocado.checkoutcounter.objects.Product;
import com.mdpcolocado.checkoutcounter.promos.IScannedProducts;

public class ReceiptBuilder {
	private final static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.00"); 
	private double total;
	private Deque<Product> scannedProducts;
	public ReceiptBuilder() {
		total = 0;
		scannedProducts = new ArrayDeque<>();
	}
	
	public double addProductAndGetTotal(long productId) {
		if (DBUtils.getInstance().hasProduct(productId)) {
			Product product = DBUtils.getInstance().getCopyOfProduct(productId);
			product.setPromotions(DBUtils.getInstance().getPromotionsOf(product.getId()));
			scannedProducts.add(product);
			product.getPromotions().forEach(p -> {
				if (p instanceof IScannedProducts) {
					((IScannedProducts) p).setScannedProducts(product, new ArrayDeque<>(scannedProducts));
				}
			});
			product.setComputedPrice(product.getPromotions().isEmpty() ? product.getPrice() : product.getPromoPrice());
			total += product.getComputedPrice();
		}
		return total;
	}
	
	/**
	 * @param productId
	 * @param weightOrVolume - weight is expected to be kilogram/s and volume is expected to be liter/s
	 * @return
	 */
	public double addProductAndGetTotal(long productId, float weightOrVolume) {
		if (DBUtils.getInstance().hasProduct(productId)) {
			Product product = DBUtils.getInstance().getCopyOfProduct(productId);
			product.setPromotions(DBUtils.getInstance().getPromotionsOf(product.getId()));
			product.setScannedWeightOrVolume(weightOrVolume);
			switch(product.getProductUnit()) {
				case PIECE:
					return addProductAndGetTotal(productId);
				case LITER:
				case KILO:
					scannedProducts.add(product);
					product.getPromotions().forEach(p -> {
						if (p instanceof IScannedProducts) {
							((IScannedProducts) p).setScannedProducts(product, new ArrayDeque<>(scannedProducts));
						}
					});
					product.setComputedPrice(product.getPromotions().isEmpty() ? product.getPrice() : product.getPromoPrice() * weightOrVolume);
					total += product.getComputedPrice();
			}
		}
		return total;
	}

	public double getTotal() {
		return total;
	}

	public Deque<Product> getScannedProducts() {
		return scannedProducts;
	}
	
	public void clear() {
		total = 0;
		scannedProducts.clear();
	}
	
	public void build() {
		scannedProducts.forEach(p -> {
			System.out.println(p.getName() + " : " + DECIMAL_FORMAT.format(p.getComputedPrice()));
		});
		System.out.println("Total : " + DECIMAL_FORMAT.format(total));
	}

}
