package com.mdpcolocado.checkoutcounter.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.mdpcolocado.checkoutcounter.objects.Product;
import com.mdpcolocado.checkoutcounter.promos.IPromotion;

public class DBUtils {

	private static DBUtils dbUtils = new DBUtils();
	
	private long recentId;
	private Map<Long, Product> productMap;
	private Map<Long, List<IPromotion>> promoMap;
	
	private DBUtils() {
		productMap = new HashMap<>();
		promoMap = new HashMap<>();
		recentId = 0;
	}
	
	public final static DBUtils getInstance() {
		if (dbUtils == null) {
			dbUtils = new DBUtils();
		}
		
		return dbUtils;
	}
	
	public Product getProduct(long id) {
		return productMap.getOrDefault(id, null);
	}
	
	public Product getCopyOfProduct(long id) {
		return new Product.Builder(getProduct(id)).build();
	}
	
	public Product getProduct(String name) {
		return productMap.values()
				.stream()
				.filter(p -> p.getName().equalsIgnoreCase(name))
				.collect(Collectors.toList()).get(0);
	}
	
	public ArrayList<Product> getAllProducts() {
		return new ArrayList<Product>(productMap.values());
	}
	
	public List<IPromotion> getPromotionsOf(long id) {
		List<IPromotion> list = new LinkedList<>();
		if (promoMap.containsKey(id)) {
			promoMap.get(id).forEach(p -> list.add(p.getDeepCopy()));
		}
		return list;
	}
	
	public long getRecentId() {
		return recentId;
	}
	
	public long incrementAndGetRecentId() {
		recentId++;
		return recentId;
	}
	
	public boolean hasProduct(long id) {
		return productMap.containsKey(id);
	}
	
	public void clearAll() {
		productMap = new HashMap<>();
		promoMap = new HashMap<>();
		recentId = 0;
	}
	
	/**
	 * @param product
	 * 
	 * add given product to db
	 * replace the product on db if existing
	 */
	public void addProduct(Product product) {
		productMap.put(product.getId(), product);
	}
	
	public void addPromotion(long productId, IPromotion promo) {
		if (!promoMap.containsKey(productId)) {
			promoMap.put(productId, new LinkedList<>());
		}
		promoMap.get(productId).add(promo);
	}
	
}
