import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.mdpcolocado.checkoutcounter.objects.Product;
import com.mdpcolocado.checkoutcounter.objects.ProductUnit;
import com.mdpcolocado.checkoutcounter.promos.BOGOPromo;
import com.mdpcolocado.checkoutcounter.promos.CustomPromo;
import com.mdpcolocado.checkoutcounter.promos.DiscountPromo;
import com.mdpcolocado.checkoutcounter.promos.IPromotion;
import com.mdpcolocado.checkoutcounter.utils.DBUtils;
import com.mdpcolocado.checkoutcounter.utils.ReceiptBuilder;

class CheckoutCounterTest {

	private final static long BEER_ID = 1;
	private final static long MOUSE_ID = 2;
	private final static long RICE_ID = 3;
	private final static long KEYBOARD_ID = 4;
	
	private final static double BEER_PRICE = 50;
	private final static double MOUSE_PRICE = 1000;
	private final static double RICE_PRICE = 70;
	private final static double KEYBOARD_PRICE = 1500;
	ReceiptBuilder receiptBuilder;
	
	@BeforeEach
	void initProducts() {
		Product beer = new Product.Builder("Beer", BEER_PRICE, ProductUnit.PIECE).description("b").build();
		Product mouse = new Product.Builder("Mouse", MOUSE_PRICE, ProductUnit.PIECE).build();
		Product rice = new Product.Builder("Rice", RICE_PRICE, ProductUnit.KILO).build();
		Product keyboard = new Product.Builder("Keyboard", KEYBOARD_PRICE, ProductUnit.KILO).build();
		
		DBUtils.getInstance().addProduct(beer);
		DBUtils.getInstance().addProduct(mouse);
		DBUtils.getInstance().addProduct(rice);
		DBUtils.getInstance().addProduct(keyboard);

		DBUtils.getInstance().addPromotion(keyboard.getId(), new DiscountPromo(keyboard.getId(), 200, 0, ""));
		DBUtils.getInstance().addPromotion(beer.getId(), new BOGOPromo("Buy one get one free beer"));
		DBUtils.getInstance().addPromotion(rice.getId(), new BOGOPromo("desc"));
		DBUtils.getInstance().addPromotion(mouse.getId(), new DiscountPromo(mouse.getId(), 0, 50f, ""));
		
		IPromotion bogoMouse = new CustomPromo.Builder("").getFree().limitTo(2).build();
		
		DBUtils.getInstance().addPromotion(mouse.getId(), bogoMouse);
		receiptBuilder = new ReceiptBuilder();
		assertEquals(4, DBUtils.getInstance().getAllProducts().size());
	}
	
	@Test
	void testBOGOPromo() {
		
		printScannedProduct(BEER_ID, receiptBuilder.addProductAndGetTotal(BEER_ID));
		printScannedProduct(BEER_ID, receiptBuilder.addProductAndGetTotal(BEER_ID));
		printScannedProduct(BEER_ID, receiptBuilder.addProductAndGetTotal(BEER_ID));
		printScannedProduct(BEER_ID, receiptBuilder.addProductAndGetTotal(BEER_ID));
		printScannedProduct(BEER_ID, receiptBuilder.addProductAndGetTotal(BEER_ID));
		
		assertEquals(150, receiptBuilder.getTotal());
	}
	
	@Test
	void testDiscountPromo() {
		printScannedProduct(KEYBOARD_ID, receiptBuilder.addProductAndGetTotal(KEYBOARD_ID));
		
		assertEquals(1300, receiptBuilder.getTotal());
	}
	
	@Test
	void testCustomBogoPromoWithDiscount() {
		
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		printScannedProduct(MOUSE_ID, receiptBuilder.addProductAndGetTotal(MOUSE_ID));
		
		assertEquals(1500, receiptBuilder.getTotal());
	}
	
	private void printScannedProduct(long id, double total) {
		Product product = DBUtils.getInstance().getProduct(id);
		System.out.println(product.getName() + " - Total : " + total);
	}
	
	
	@AfterEach
	void cleanup() {
		System.out.println("------RECEIPT-------");
		receiptBuilder.build();
		DBUtils.getInstance().clearAll();
		receiptBuilder.clear();
		
		assertEquals(0, DBUtils.getInstance().getAllProducts().size());
		assertEquals(0, receiptBuilder.getScannedProducts().size());
		System.out.println("---------------------------------------------------------");
	}
}
